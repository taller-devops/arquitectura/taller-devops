const express = require("express");
const router = express.Router();

/* GET home page. */
router.get("/", function (req, res, next) {
  res.json({ text: "Hola este es una prueba con nodejs!" });
});

module.exports = router;
