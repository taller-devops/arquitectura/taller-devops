const express = require("express");
const router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.json([
    { name: "kevin", age: 25 },
    { name: "Juan", age: 26 },
    { name: "Miguel", age: 27 },
    { name: "Fatima", age: 28 },
    { name: "Carolina", age: 30 },
    { name: "Jose", age: 21 },
    { name: "Alicia", age: 5 },
    { name: "Mauricio", age: 25 },
    { name: "Jacobo", age: 27 },
    { name: "Rosa", age: 29 }
  ]);
});

module.exports = router;

