const request = require("supertest");
const app = require("../server/app.js");

describe("GET /users", function () {
  it("return list of users", function () {
    return request(app)
      .get("/users")
      .expect(200)
      .expect("Content-Type", /json/)
      .expect(
        '[{"name":"kevin","age":25},{"name":"Juan","age":26},{"name":"Miguel","age":27},{"name":"Fatima","age":28},{"name":"Carolina","age":30},{"name":"Jose","age":21},{"name":"Alicia","age":5},{"name":"Mauricio","age":25},{"name":"Jacobo","age":27},{"name":"Rosa","age":29}]'
      );
  });
});
